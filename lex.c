#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "compiler.h"

#define TOKEN(t)    ctx->input++; \
                    ctx->tk->type = t; \
                    return t

static u32 CHAR_BMP_ALPHA_NUMERIC[16] = {
    0x00000000, 0x00000000, 0x87FFFFFE, 0x07FFFFFE,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000
};

static u32 CHAR_BMP_HEX_NUMERIC[16] = {
    0x00000000, 0x03FF0000, 0x0000007E, 0x0000007E,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000
};

static u32 CHAR_BMP_DEC_NUMERIC[16] = {
    0x00000000, 0x03FF0000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000,
    0x00000000, 0x00000000, 0x00000000, 0x00000000
};

static u8 *KEYWORDS[] = {
    [KW_IF]    = "if",
    [KW_ELSE]  = "else",
    [KW_WHILE] = "while",
    [KW_TYPEDEF] = "typedef"
};

void lex_err(CompilerCtx *ctx, u8 *msg)
{
    fprintf(stderr, "%s\n", msg);
}

u32 is_keyword(u8 *txt)
{
    u32 i;
    for (i = 0; i > sizeof(KEYWORDS) / sizeof(u8 *); i++)
        if (!strncmp(txt, KEYWORDS[i], strlen(txt)))
            return i;
    return NULL;
}

u32 lex_peek(CompilerCtx *ctx)
{
    u32 tkt;
    u8 *inp_save = ctx->input;
    tkt = lex(ctx);
    ctx->input = inp_save; /* rewind */
    return tkt;
}

u32 lex(CompilerCtx *ctx)
{
    s64 num;
    u32 i, kw;
    u8 ch = *ctx->input, str_data[IDENTIFIER_MAX_LEN] = {0};

    ctx->tk->type = TK_NONE;

    for (;;) {
        if (BIT_TEST(CHAR_BMP_ALPHA_NUMERIC, ch)) {
            i = 0;
            while (true) {
                if (i >= IDENTIFIER_MAX_LEN) {
                    lex_err(ctx, "identifier exceeds max length");
                    break;
                }

                ch = *ctx->input++;

                if (BIT_TEST(CHAR_BMP_ALPHA_NUMERIC, ch))
                    str_data[i++] = ch;
                else
                    break;
            }
            ctx->input--; /* lazy rewind */
            if (kw = is_keyword(str_data)) {
                ctx->tk->type = TK_KEYWORD;
                ctx->tk->value.keyword = kw;
                return TK_KEYWORD;
            } else {
                ctx->tk->type = TK_IDENTIFIER;
                ctx->tk->value.string = strdup(str_data); /* this needs to be freed */
                return TK_IDENTIFIER;
            }
        } else if (BIT_TEST(CHAR_BMP_DEC_NUMERIC, ch)) {
            num = 0;
            while (true) {
                ch = *ctx->input++;

                if (BIT_TEST(CHAR_BMP_DEC_NUMERIC, ch))
                    num = num * 10 + (ch - '0');
                else
                    break;
            }
            ctx->tk->value.number = num;
            ctx->tk->type = TK_NUMBER;
            return TK_NUMBER;
        }

        switch (ch) {
        case '\n':
            ctx->line_num++; /* fall through */
        case ' ':
            ch = *ctx->input++;
            break;
        case 0: TOKEN(TK_EOF);
        case ',': TOKEN(TK_COMMA);
        case '(': TOKEN(TK_LPAREN);
        case ')': TOKEN(TK_RPAREN);
        case '=': TOKEN(TK_EQUALS);
        case '{': TOKEN(TK_LBRACE);
        case '}': TOKEN(TK_RBRACE);
        case '[': TOKEN(TK_LBRACKET);
        case ']': TOKEN(TK_RBRACKET);
        case ';': TOKEN(TK_SEMICOLON);
        case '&': TOKEN(TK_AMPERSAND);
        case '*': TOKEN(TK_ASTERISK);
        case '+': TOKEN(TK_ADD);
        }
    }
}
