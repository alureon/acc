#ifndef COMPILER_H
#define COMPILER_H
#include "types.h"

#define TK_NONE         0
#define TK_IDENTIFIER   1
#define TK_NUMBER       2
#define TK_COMMA        3
#define TK_LPAREN       4
#define TK_RPAREN       5
#define TK_EQUALS       6
#define TK_LBRACE       7
#define TK_RBRACE       8
#define TK_KEYWORD      9
#define TK_STRING       10
#define TK_RBRACKET     11
#define TK_LBRACKET     12
#define TK_SEMICOLON    13
#define TK_AMPERSAND    14
#define TK_ASTERISK     15
#define TK_ADD          16
#define TK_EOF          17

#define KW_IF           0
#define KW_ELSE         1
#define KW_FOR          2
#define KW_WHILE        3
#define KW_TYPEDEF      4

#define IDENTIFIER_MAX_LEN  256

#define BIT_TEST(a, k)  ( a[(k/32)] & (1 << (k%32)) )

typedef struct {
    u32 type;
    union {
        s64 number;
        u8 *string;
        u32 keyword;
    } value;
} Token;

typedef struct {
    u8 *output,
       *input;
    Token *tk;
    u64 line_num;
} CompilerCtx;

u32 lex(CompilerCtx *ctx);
u32 lex_peek(CompilerCtx *ctx);
CompilerCtx *cc_new();

#endif /* COMPILER_H */
