#include <stdio.h>
#include "compiler.h"
#include "parse.h"

int main()
{
    ASTNode *node;
    CompilerCtx *ctx = cc_new();
    char *data = "1 + 1 + 2";
    ctx->input = data;
    node = parse_expression(ctx);
    ast_print(node, 0);
    // printf("%d\n", node->type);
}
