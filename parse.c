#include <stdio.h>
#include <stdlib.h>
#include "parse.h"

static void parse_error(CompilerCtx *ctx, char *msg)
{
    fprintf(stderr, "PARSE ERROR AT LINE %d: %s\n", ctx->line_num, msg);
}

static ASTNode *node_new(u32 max_children, u32 type)
{
    ASTNode *node;
    if (!(node = malloc(sizeof(ASTNode))))
        return NULL;
    node->type = type;
    node->max_children = max_children;
    node->num_children = 0;
    if (max_children > 0)
        node->children = malloc(sizeof(ASTNode *) * max_children);
    return node;
}

static void node_add_child(ASTNode *parent, ASTNode *child)
{
    if (parent->num_children + 1 > parent->max_children)
        return;
    parent->children[parent->num_children++] = child;
}

static void parse_expect(CompilerCtx *ctx, u32 type)
{
    if (lex(ctx) != type)
        parse_error(ctx, "expected a symbol that we didn't get");
}

/* i'm thinking just change this to print_node, then
 * we can easily print out the number along with the type, etc
 */
static char *node_to_str(ASTNode *node)
{
    switch (node->type) {
    case NODE_UNASSIGNED:     return "nop";
    case NODE_LITERAL_NUMBER: return "number";
    case NODE_LITERAL_STRING: return "string";
    case NODE_ASSIGNMENT:     return "assignment";
    case NODE_OPERATION:
        if (node->value.operation == OPERATION_UNARY_DEREF)
            return "deref";
        else if (node->value.operation == OPERATION_UNARY_ADDROF)
            return "addrof";
        return "operation";
    }
}

void ast_print(ASTNode *root, u32 indent)
{
    u32 i, j;

    for (j = 0; j < indent; j++)
        printf("\t");
    printf("%s\n", node_to_str(root));
    for (i = 0; i < root->num_children; i++)
        ast_print(root->children[i], indent + 1);
}

/*
 * i'm confused on parsing the grammar
 * expr -> [ -> expr -> ]
 * if the first expr is an identifier (variable), etc
 * then we have to parse it, in parse_expression.
 * however, to get to the 'default' case, which recursively calls
 * parse_expression in the index grammar parsing, we would need to
 * skip over it.
*/
ASTNode *parse_expression(CompilerCtx *ctx)
{
    ASTNode *node, *node2;

    switch (lex(ctx)) {
    case TK_NUMBER:
        node = node_new(0, NODE_LITERAL_NUMBER);
        node->value.number = ctx->tk->value.number;
        /* maybe we should make a TK_OP, then in the token define the operation type */
        if (lex_peek(ctx) == TK_ADD) {
            lex(ctx);
            node2 = node_new(2, NODE_OPERATION);
            node2->value.operation = OPERATION_ADD;
            node_add_child(node2, node);
            node_add_child(node2, parse_expression(ctx)); /* idk if this is correct */
            return node2;
        }
        break;
    case TK_STRING: /* contemplating even having a string, we can just do: ",ident," */
        node = node_new(0, NODE_LITERAL_STRING);
        node->value.string = ctx->tk->value.string;
        break;
    case TK_AMPERSAND:
        node = node_new(1, NODE_OPERATION);
        node->value.operation = OPERATION_UNARY_ADDROF;
        node_add_child(node, parse_expression(ctx));
        break;
    case TK_ASTERISK:
        node = node_new(1, NODE_OPERATION);
        node->value.operation = OPERATION_UNARY_DEREF;
        node_add_child(node, parse_expression(ctx));
        break;
    case TK_IDENTIFIER:
        /* this is a var or type */
        break;
    case TK_LPAREN:
        node = parse_expression(ctx);
        if (lex(ctx) != TK_RPAREN)
            parse_error(ctx, "expected ')'");
        break;
    default: /* none of above, index or function call */
        node = node_new(2, NODE_UNASSIGNED);
        node_add_child(node, parse_expression(ctx));
        lex(ctx);
        switch (lex(ctx)) {
        case TK_LPAREN: /* function call */
            if (lex(ctx) != TK_RPAREN)
                parse_error(ctx, "expected ')'");
            break;
        case TK_LBRACKET: /* array index */
            node2 = node_new(1, NODE_OPERATION);
            node2->value.operation = OPERATION_UNARY_DEREF;
            node->type = NODE_OPERATION;
            node->value.operation = OPERATION_ADD;
            node_add_child(node, parse_expression(ctx));
            node_add_child(node2, node);
            if (lex(ctx) != TK_RBRACKET)
                parse_error(ctx, "expected ']'");
            return node2;
        case TK_AMPERSAND:
            if (lex(ctx) == TK_AMPERSAND) {
                node->type = NODE_OPERATION;
                node->value.operation = OPERATION_LOGICAL_AND;
                node_add_child(node, parse_expression(ctx));
            } else {
                /* binary AND */
            }
            break;
        }
        break;
    }
    return node;
}

ASTNode *parse_statement(CompilerCtx *ctx)
{
    ASTNode *node;

    switch (lex(ctx)) {
    case TK_LBRACE:
        parse_statement(ctx);
        if (lex(ctx) != TK_RBRACE)
            parse_error(ctx, "expected '}'");
        break;
    case TK_KEYWORD:
        node = node_new(2, NODE_UNASSIGNED);
        switch (ctx->tk->value.keyword) {
        case KW_IF:
           if (lex(ctx) != TK_LPAREN)
                parse_error(ctx, "expected '('");
            parse_expression(ctx);
            if (lex(ctx) != TK_RPAREN)
                parse_error(ctx, "expected ')'");
            parse_statement(ctx);
            break;
        case KW_TYPEDEF:
            break;
        }
        /*
        if (ctx->tk->value.keyword == KW_IF) {
            if (lex(ctx) != TK_LPAREN)
                parse_error(ctx, "expected '('");
            parse_expression(ctx);
            if (lex(ctx) != TK_RPAREN)
                parse_error(ctx, "expected ')'");
            parse_statement(ctx);
        } else if (ctx->tk->value.keyword == KW_WHILE) {
        }
        */
        break;
    case TK_SEMICOLON:
        /* nop */
        break;
    default: /* none of these, must be expr */
        node = parse_expression(ctx);
        if (lex(ctx) != TK_SEMICOLON)
            parse_error(ctx, "expected ';'");
    }
}

