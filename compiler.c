#include <stdlib.h>
#include "compiler.h"

CompilerCtx *cc_new()
{
    CompilerCtx *ctx;
    if (!(ctx = malloc(sizeof(CompilerCtx))))
        return NULL;
    ctx->line_num = 1;
    ctx->tk = malloc(sizeof(Token));
    return ctx;
}
