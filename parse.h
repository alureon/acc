#ifndef PARSE_H
#define PARSE_H
#include "compiler.h"

typedef struct ASTNode {
    enum {
        NODE_UNASSIGNED,
        NODE_LITERAL_NUMBER,
        NODE_LITERAL_STRING,
        NODE_ASSIGNMENT,
        NODE_EXPRESSION,
        NODE_VARIABLE,
        NODE_STATEMENT,
        NODE_OPERATION
    } type;
    u32 num_children,
        max_children;
    struct ASTNode **children;
    union {
        s64 number;
        u8 *string;
        enum {
            OPERATION_ADD,
            OPERATION_LOGICAL_AND,
            OPERATION_LOGICAL_OR,
            OPERATION_UNARY_DEREF,
            OPERATION_UNARY_ADDROF
        } operation;
    } value;
} ASTNode;

void ast_print(ASTNode *root, u32 indent);
ASTNode *parse_expression(CompilerCtx *ctx);

#endif /* PARSE_H */
